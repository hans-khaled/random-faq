-- FAQ DATABASE TABLES STRUCTURES

-- admin table
CREATE TABLE IF NOT EXISTS admins(
    id INT GENERATED ALWAYS AS IDENTITY,
    _uuid uuid DEFAULT uuid_generate_v4(),
    email VARCHAR(100) NOT NULL,
    _hash VARCHAR(255) NOT NULL,
    _salt VARCHAR(255) NOT NULL,
    lname VARCHAR(255) NOT NULL,
    fname VARCHAR(255) NOT NULL,
-- Non pris en compte pour le moment
--     rights JSON NOT NULL,
    created_at TIMESTAMPTZ NOT NULL DEFAULT now(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT now(),
    created_by INT,
    updated_by INT,
    CONSTRAINT pk_admins_id PRIMARY KEY(id),
    CONSTRAINT fk_created_by_id 
        FOREIGN KEY(created_by)
            REFERENCES admins(id)
            ON DELETE SET NULL
            ON UPDATE CASCADE,
    CONSTRAINT fk_update_by_id
        FOREIGN KEY(updated_by)
            REFERENCES admins(id)
            ON DELETE SET NULL
            ON UPDATE CASCADE,
    CONSTRAINT u_email UNIQUE(email)
);

-- question table
CREATE TABLE IF NOT EXISTS questions(
    id INT GENERATED ALWAYS AS IDENTITY,
    title VARCHAR(255) NOT NULL,
    body TEXT DEFAULT NULL,
    author_email VARCHAR(100) DEFAULT NULL,
    answer TEXT DEFAULT NULL, 
    created_at TIMESTAMPTZ DEFAULT now(),
    updated_at TIMESTAMPTZ DEFAULT now(),
    updated_by INT DEFAULT NULL,
    answered_at TIMESTAMPTZ DEFAULT NULL,
    answered_by INT DEFAULT NULL,
    CONSTRAINT pk_questions_id PRIMARY KEY(id),
    CONSTRAINT fk_answered_by_id
        FOREIGN KEY(answered_by)
            REFERENCES admins(id)
            ON UPDATE CASCADE
            ON DELETE SET NULL,
    CONSTRAINT fk_updated_by_id
        FOREIGN KEY(updated_by)
            REFERENCES admins(id)
            ON UPDATE CASCADE
            ON DELETE SET NULL
);

-- tags table
CREATE TABLE tags(
    tag VARCHAR(50),
    created_at TIMESTAMPTZ DEFAULT now(),
    CONSTRAINT pk_tag PRIMARY KEY(tag)
);

-- questions_tags table
CREATE TABLE questions_tags(
    question INT NOT NULL,
    tag VARCHAR(50) NOT NULL,
    CONSTRAINT fk_question_id
        FOREIGN KEY(question)
            REFERENCES questions(id)
            ON UPDATE CASCADE
            ON DELETE CASCADE,
    CONSTRAINT fk_tag_tag
        FOREIGN KEY(tag)
            REFERENCES tags(tag)
            ON UPDATE CASCADE
            ON DELETE CASCADE,
    CONSTRAINT u_question_tag UNIQUE(question, tag)
);
