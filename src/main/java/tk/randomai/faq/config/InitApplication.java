/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tk.randomai.faq.config;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import tk.randomai.faq.dao.DaoFactory;


/**
 * This class is made to run every thing necessary before
 * the application start.
 * @author kadou
 */

public class InitApplication implements ServletContextListener{
    private DaoFactory factory;
    
    @Override
    public void contextInitialized(ServletContextEvent event) {
        ServletContext context = event.getServletContext();
        
        // We get and set the dao factory as an application attribute
        this.factory = DaoFactory.getInstance();
        context.setAttribute("dao_factory", this.factory);
    }
    
    @Override
    public void contextDestroyed(ServletContextEvent event) {
        // Nothing to do here
    }
    
    
}
