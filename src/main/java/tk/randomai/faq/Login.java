package tk.randomai.faq;

import java.io.IOException;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import tk.randomai.faq.beans.Admin;
import tk.randomai.faq.dao.AdminDao;
import tk.randomai.faq.dao.DaoFactory;
import tk.randomai.faq.util.Security;
import javax.servlet.http.HttpSession;
import tk.randomai.faq.dao.exceptions.DaoException;

public class Login extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // We check if the default admin account is set up.
        // If not, we do it.
        DaoFactory factory = (DaoFactory) getServletContext()
                .getAttribute("dao_factory");
        Connection connection = null;
        try {
            connection = factory.connect();
        }
        catch (DaoException ex) {
            throw ex;
        }
        PreparedStatement statement = null;
        ResultSet result = null;
        try {
            statement = DaoFactory.initPreparedStatement(connection,
                    true, "SELECT * FROM admins WHERE email = ?",
                    "admin@random.ai");
            result = statement.executeQuery();
            if (result.next()) {
                // The admin account exists, so we do nothing
                Logger.getLogger(Login.class.getName())
                        .log(Level.INFO, "Default admin account exists.");
                Logger.getLogger(Login.class.getName())
                        .log(Level.INFO, "Admin Account ID::= {0}", result.getLong("id"));
            } else {
                HashMap<String, String> hash = Security.hash("admin");
                statement = DaoFactory.initPreparedStatement(connection, true,
                        "INSERT INTO admins(email, _hash, lname, fname) VALUES (?, ?, ?, ?)",
                        "admin@random.ai", hash.get("hash"), "Admin", "admin");
                int status = statement.executeUpdate();
                if (status == 0) {
                    Logger.getLogger(Login.class.getName())
                            .log(Level.WARNING, "Default admin account doesn't exist.\nTrying to create one: failed.");
                }
                result = statement.getGeneratedKeys();
                if (result.next()) {
                    Logger.getLogger(Login.class.getName())
                            .log(Level.INFO, "Default admin account doesn't exist.\nTrying to create one: succeeded.");
                    Logger.getLogger(Login.class.getName())
                            .log(Level.INFO, "Admin Account ID::= {0}", result.getLong("id"));
                }
            }
        }
        catch (SQLException e) {
            Logger.getLogger(Login.class.getName())
                    .log(Level.WARNING, "Unable to setup default admin account.: {}", e);

        }
        String source = request.getParameter("from");
        System.out.println(request.getRequestURL());
        /*
        * La servlet correspond à plusieurs routes, 
        * il faut donc que l'on mette le bon endpoint pour
        * le formulaire de la vue.
         */
        String url = request.getScheme() + "://"
                + request.getServerName()
                + ("http".equals(request.getScheme()) && request.getServerPort() == 80
                || "https".equals(request.getScheme())
                && request.getServerPort() == 443 ? "" : ":"
                + request.getServerPort())
                + request.getContextPath()
                + "/management";
        request.setAttribute("submitEndpoint", url);
        if (source != null) {
            request.setAttribute("headerMessage",
                    "Connectez vous avant de continuer");
            request.setAttribute("from", source);
            request.setAttribute("submitEndpoint", "");
        }
        this.getServletContext().getRequestDispatcher("/WEB-INF/login.jsp")
                .forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getParameter("submit") != null) {
            String email = (String) request.getParameter("email");
            String password = (String) request.getParameter("password");
            DaoFactory factory = (DaoFactory) getServletContext()
                    .getAttribute("dao_factory");
            AdminDao adminDao;
            adminDao = factory.getAdminDao();
            Admin user = adminDao.findbyEmail(email);
            boolean ch = false;
            if (user != null) {
                ch = Security.check(password, user.getHash());
            } else {
                request.setAttribute("headerMessage",
                        "Identifiants incorrects");
                this.getServletContext()
                        .getRequestDispatcher("/WEB-INF/login.jsp")
                        .forward(request, response);
            }
            if (user != null && ch) {
                HttpSession session = request.getSession();
                session.setAttribute("user", user);
                session.setMaxInactiveInterval(2400);
                String source = request.getParameter("from");
                if (source != null) {
                    String decodedUrl = URLDecoder.decode(source);
                    try {
                        response.sendRedirect(decodedUrl);
                    }
                    catch (IOException e) {
                        response.sendRedirect(
                                request.getContextPath() + "/management/dashboard");
                    }
                } 
                else {
                    response
                        .sendRedirect(request.getContextPath() + "/management/dashboard");
                }

            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
