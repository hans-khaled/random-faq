/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tk.randomai.faq;

import java.io.IOException;
import java.net.URLEncoder;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import tk.randomai.faq.dao.DaoFactory;
import tk.randomai.faq.dao.QuestionDao;

/**
 *
 * @author kadou
 */
public class Dashboard extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String from = request.getRequestURL().toString();
        String context = request.getScheme() + "://"
                + request.getServerName()
                + ("http".equals(request.getScheme()) && request.getServerPort() == 80
                || "https".equals(request.getScheme())
                && request.getServerPort() == 443 ? "" : ":"
                + request.getServerPort())
                + request.getContextPath();
        if (request.getSession().getAttribute("user") == null) {
            String url = context + "/management/login?from="
                    + URLEncoder.encode(from);
            response.sendRedirect(url);
        } else {
            request.setAttribute("contextPath", context);
            int offset = 0;
            try {
                offset = Integer.parseInt(request.getParameter("offset"));
            }
            catch (Exception e) {
            }
            DaoFactory factory = (DaoFactory) getServletContext()
                    .getAttribute("dao_factory");
            QuestionDao qDao = factory.getQuestionDao();
            int total = qDao.count();
            request.setAttribute("total", total);
            try {
                request.setAttribute("questions", qDao.getQuestions(20, offset, false));
            }
            catch (Exception e) {
            }
            this.getServletContext()
                    .getRequestDispatcher("/WEB-INF/dashboard.jsp")
                    .forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
