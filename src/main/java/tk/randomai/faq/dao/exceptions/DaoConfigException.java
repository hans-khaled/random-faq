/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tk.randomai.faq.dao.exceptions;

/**
 *
 * @author kadou
 */
public class DaoConfigException extends RuntimeException {

    public DaoConfigException() {
    }

    public DaoConfigException(String string) {
        super(string);
    }

    public DaoConfigException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

    public DaoConfigException(Throwable thrwbl) {
        super(thrwbl);
    }
}
