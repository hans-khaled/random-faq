/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tk.randomai.faq.dao.interfaces;

import java.util.List;
import tk.randomai.faq.beans.Question;
import tk.randomai.faq.beans.Tag;
import tk.randomai.faq.dao.exceptions.DaoException;

/**
 *
 * @author kadou
 */
public interface QuestionDaoInterface {

    List<Question> getQuestions(int limit, int offset, boolean answered) throws DaoException;
    List<String> getTags(Question question) throws DaoException;

    List<Question> getQuestions(int limit, int offset, List<String> tags) 
            throws DaoException;

    void store(Question question) throws DaoException;

    Question getById(int id);

    List<Question> findByTag(List<Tag> tags) throws DaoException;

    void delete(Question question) throws DaoException;
}
