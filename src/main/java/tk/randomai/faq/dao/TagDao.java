/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tk.randomai.faq.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import tk.randomai.faq.beans.Tag;
import tk.randomai.faq.dao.exceptions.DaoException;

/**
 *
 * @author tobi
 */
public class TagDao {

    private static final String INSERT_TAG = "INSERT INTO tags(tag) VALUES (?)";
    private static final String FIND_TAG = "SELECT tag FROM tags WHERE tag = ?";
    private DaoFactory factory;

    public TagDao(DaoFactory factory) {
        this.factory = factory;
    }

    public void addTag(Tag tag) {
        if (getTag(tag.getTag()) == null) {
            Connection connection = null;
            try {
                connection = factory.connect();
            }
            catch (DaoException ex) {
                throw ex;
            }
            PreparedStatement statement = null;
            ResultSet generatedKeys = null;
            int status;
            try {
                statement = DaoFactory.initPreparedStatement(connection,
                        true, INSERT_TAG,
                        tag.getTag());
            }
            catch (SQLException e) {
                throw new DaoException("Something wents wrong", e);
            }

            try {
                status = statement.executeUpdate();
            }
            catch (SQLException e) {
                throw new DaoException("Unable to add tag", e);
            }

            if (status == 0) {
                throw new DaoException("Unable to add tag");
            }
            DaoFactory.closeSilently(connection, statement, generatedKeys);
        }
    }

    public Tag getTag(String tag) {
        Tag tagObj = null;
        Connection connection = null;
        try {
            connection = factory.connect();
        }
        catch (DaoException e) {
            throw e;
        }
        PreparedStatement statement = null;
        ResultSet result = null;
        try {
            statement = DaoFactory.initPreparedStatement(connection,
                    true, FIND_TAG, tag);
            result = statement.executeQuery();
            if (result.next()) {
                tagObj = new Tag();
                tagObj.setTag(result.getString("tag"));
                tagObj.setCreated_at(result.getString("created_at"));
            }
            DaoFactory.closeSilently(connection, statement, result);
        }
        catch (SQLException e) {
            DaoFactory.closeSilently(connection, statement, result);
            throw new DaoException("Something went wrong", e);
        }
        return tagObj;
    }
}
