package tk.randomai.faq.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import tk.randomai.faq.beans.Admin;
import tk.randomai.faq.dao.exceptions.DaoException;
import tk.randomai.faq.dao.interfaces.AdminDaoInterface;

public class AdminDao implements AdminDaoInterface {
    public static final String FIND_ADMIN_SQL_QUERY = "SELECT * FROM admins WHERE"
        + " email = ?";
    private static final String INSERT_NEW_ADMIN_SQL = "INSERT INTO admins(email,"
            + " _hash, lname, fname, created_by) VALUES "
            + "(?, ?, ?, ?, ?)";
    private DaoFactory factory;

    public AdminDao(DaoFactory factory) {
        this.factory = factory;
    }
    
    @Override
    public Admin findbyEmail(String email) {
        Admin admin = null;
        Connection connection = null;
        try {
            connection = factory.connect();
        }
        catch (DaoException ex) {
            throw ex;
        }
        PreparedStatement statement = null;
        ResultSet result = null;
        try {
            statement = DaoFactory.initPreparedStatement(connection,
                    true, FIND_ADMIN_SQL_QUERY,
                    email);
        }
        catch (SQLException e) {
            throw new DaoException("Something wents wrong", e);
        }
        try {
            result = statement.executeQuery();
            if (result.next()) {
                admin = new Admin();
                admin.setId(result.getLong("id"));
                admin.setUuid(result.getString("_uuid"));
                admin.setEmail("email");
                admin.setHash(result.getString("_hash"));
                admin.setFname("fname");
                admin.setLname("lname");
                admin.setCreated_at(result.getString("created_at"));
                admin.setUpdated_at(result.getString("updated_at"));
                admin.setCreated_by(result.getLong("created_by"));
                admin.setUpdated_by(result.getLong("updated_by"));
            }
        }
        catch (SQLException e) {
            throw new DaoException("Something wents wrong", e);
        }
        return admin;
    }
    
    public void store(Admin admin) {
        if (admin == null)
            return;
        Connection connection = null;
        try {
            connection = factory.connect();
        }
        catch (DaoException ex) {
            DaoFactory.closeSilently(connection);
            throw ex;
        }
        PreparedStatement statement = null;
        ResultSet generatedKeys = null;
        int status;
        try {
            statement = DaoFactory.initPreparedStatement(connection,
                    true, INSERT_NEW_ADMIN_SQL, 
                    admin.getEmail(),
                    admin.getHash(),
                    admin.getLname(),
                    admin.getFname(),
                    admin.getCreated_by());
        }
        catch (SQLException e) {
            DaoFactory.closeSilently(connection, statement, generatedKeys);
            throw new DaoException("Something wents wrong", e);
        }

        try {
            status = statement.executeUpdate();
        }
        catch (SQLException e) {
            DaoFactory.closeSilently(connection, statement, generatedKeys);
            throw new DaoException("Unable to create new admin.", e);
        }

        if (status == 0) {
            throw new DaoException("Unable to create new admin");
        }

        try {
            generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                admin.setId(generatedKeys.getLong("id"));
            }
        }
        catch (SQLException e) {
            DaoFactory.closeSilently(connection, statement, generatedKeys);
            throw new DaoException("Unable to create new admin:"
                    + " no id generated");
        }
        catch (DaoException e) {
            DaoFactory.closeSilently(connection, statement, generatedKeys);
            throw e;
        }
        DaoFactory.closeSilently(connection, statement, generatedKeys);
    }
}
