package tk.randomai.faq.resources;

import java.io.StringReader;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import tk.randomai.faq.beans.Question;
import tk.randomai.faq.dao.DaoFactory;
import tk.randomai.faq.dao.QuestionDao;
import javax.json.Json;
import javax.json.JsonReader;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import tk.randomai.faq.dao.exceptions.DaoConfigException;
import tk.randomai.faq.dao.exceptions.DaoException;

@Path("/questions")
@Produces("application/json")
public class QuestionResource {

    @GET
    public Response fetch(
            @DefaultValue("0") @QueryParam("offset") int offset,
            @DefaultValue("20") @QueryParam("limit") int limit,
            @DefaultValue("none") @QueryParam("tag") List<String> tags
    ) {
        System.out.println(tags);
        DaoFactory f = DaoFactory.getInstance();
        QuestionDao qDao = f.getQuestionDao();
        try {
            if (tags.get(0).equals("none") || tags.size() < 1) {
                List<Question> list = qDao.getQuestions(limit, offset, true);
                return Response.ok(list).build();
            }
            else {
                List<Question> list = qDao.getQuestions(limit, offset, tags);
                return Response.ok(list).build();
            }
        }
        catch (DaoException e) {
            JsonObjectBuilder answer = Json.createObjectBuilder()
                    .add("error", "something bad happened");
            Logger.getLogger(QuestionResource.class.getName())
                    .log(Level.WARNING, "Error when processing "
                            + "question fetch request.");
            e.printStackTrace();
            return Response.ok(answer.build()).build();
        }
//        return Response.ok(list).build();
    }

    @POST
    @Consumes("application/x-www-form-urlencoded")
    public Response ask(@FormParam("data") String data) {
        try {
            JsonReader reader = Json.createReader(new StringReader(data));
            JsonObject dataObj = reader.readObject();
            String title = "";
            String author_email = "";
            String body = "";
            try {
                title = dataObj.getString("title");
            }
            catch (Exception e) {
                // Do nothing
            }
            try {
                body = dataObj.getString("body");
            }
            catch (Exception e) {
                // Do nothing
            }
            try {
                author_email = dataObj.getString("author_email");
            }
            catch (Exception e) {
                // Do nothing
            }

            if (title.isEmpty()) {
                JsonObjectBuilder answer = Json.createObjectBuilder()
                        .add("error", "title key required");
                Logger.getLogger(QuestionResource.class.getName())
                        .log(Level.WARNING, "Error when processing "
                                + "question submit request: title key was "
                                + "missing from data provided by user");
                return Response.ok(answer.build()).build();
            } else {
                Question question = new Question();
                DaoFactory f = DaoFactory.getInstance();

                QuestionDao qDao = f.getQuestionDao();
                question.setTitle(title);
                question.setBody(body);
                question.setAuthor_email(author_email);
                qDao.store(question);
                JsonObjectBuilder answer = Json.createObjectBuilder()
                        .add("success", "Your question has been "
                                + "registered successfully")
                        .add("question_id", question.getId());
                Logger.getLogger(QuestionResource.class.getName())
                        .log(Level.INFO, "New question submitted: "
                                + "'{'id'}' -> {0}", question.getId());
                return Response.ok(answer.build()).build();
            }
        }
        catch (DaoConfigException | DaoException e) {
            JsonObjectBuilder answer = Json.createObjectBuilder()
                    .add("error", "Internal error");
            Logger.getLogger(QuestionResource.class.getName())
                    .log(Level.SEVERE, "An internal error occured when "
                            + "registering new question", e);
            return Response.ok(answer.build()).build();
        }
        catch (Exception e) {
            JsonObjectBuilder answer = Json.createObjectBuilder()
                    .add("error", "Unable to process request");
            Logger.getLogger(QuestionResource.class.getName())
                    .log(Level.WARNING, "Bad request: Unable to process "
                            + "user data to register question", e);
            return Response.ok(answer.build()).build();
        }
    }

    @GET
    @Path("/search")
    public Response search(
            @DefaultValue("") @QueryParam("q") String query,
            @DefaultValue("") @QueryParam("tag") String tag
    ) {
        return Response.ok().build();
    }

    @GET
    @Path("{id: \\d+}/answer")
    public Response getAnswer(
            @DefaultValue("-1") @PathParam("id") int id
    ) {
        return Response.ok().build();
    }

    @DELETE
    @Path("{id: \\d+}")
    public Response delete(
            @DefaultValue("-1") @PathParam("id") int id
    ) {
        try {
            DaoFactory f = DaoFactory.getInstance();
            QuestionDao qDao = f.getQuestionDao();
            Question question = qDao.getById(id);
            qDao.delete(question);
            return Response.ok(
                    Json.createObjectBuilder()
                            .add("success", "Question deleted")
                            .build()
            ).build();
        }
        catch (DaoConfigException | DaoException e) {

            return Response.ok(
                    Json.createObjectBuilder()
                            .add("error", "Question not deleted")
                            .build()
            ).build();
        }
    }
}
