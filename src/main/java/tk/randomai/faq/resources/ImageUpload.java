package tk.randomai.faq.resources;

import javax.json.JsonObject;
import javax.ws.rs.POST;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.Response;

/**
 *
 * @author kadou
 */

@Path("/images/upload")
@Produces("application/json")
public class ImageUpload {
    // Should normally be secret
    private static final String TOKEN = "4a4c97b6c0b8d6b4fecadd09ad15cec8";
    private static final String API_ENDPOINT = "https://api.imgbb.com/1/upload";
    
    
    @POST
    @Consumes("application/x-www-form-urlencoded")
    public Response upload(
        @FormParam("image") String base64Image
    ) {
        try {

            Client client = ClientBuilder.newClient();  
            WebTarget target = client.target(API_ENDPOINT);
            Invocation.Builder builder = target.request();
            Form form = new Form();
            form.param("key", TOKEN);
            form.param("image", base64Image);
            JsonObject response = builder.post(Entity.form(form), 
                JsonObject.class);
        System.out.println(response);
        return Response.ok(response).build();
        }
        catch(Exception e) {
            return Response.ok("wrong").build();
        }
    }
    
}
