<%-- 
    Document   : login
    Created on : May 4, 2021, 4:45:23 PM
    Author     : kadou
--%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Random-AI FAQ Management</title>
        <!-- Font Awesome Icon -->
        <link rel="stylesheet" href="../css/font-awesome.min.css">

        <!-- Feuille de style personnalise-->
        <link rel="stylesheet" href="../css/style_addAdmin.css">

        <script src="../js/main.js" defer></script>
    </head>
    <style>
        input[type=submit], input::placeholder {
            opacity: 1;
            color: #2e2769;
        }
        .notif {
            min-height: 30px;
            position: relative;
            display: flex;
            justify-content: center;
            align-items: center;
            padding: 8px;
            background: #2e2769;
            color: whitesmoke;
            border-radius: 5px;
            border: none;
        }
        .close-notif {
            position: absolute;
            right: 5px;
            cursor: pointer;
        }
    </style>
    <body>
        <div class="wrapper">
            <h1 style="margin: 20px auto; color: #2e2769; text-align: center; transform: scale(1.3)">Connexion</h1>
            <div class="container">
                <c:if test="${requestScope.headerMessage != null}">
                    <div id="notif" class="faq-accord notif" style="width: 100%; height: auto;position: relative">
                        <div>
                            ${requestScope.headerMessage}
                        </div>
                        <span class="close-notif" onclick="removeNotif(event)" style="float: right; color: red;">
                            <i class="fa fa-window-close"></i>
                        </span>
                    </div>
                    <br>
                    <br>
                </c:if>
                <form action="${requestScope.submitEndpoint}" method="post">
                    <c:if test="${requestScope.from != null}">
                        <input type="hidden" name="from" value="${requestScope.from}">
                    </c:if>
                    <input type="email" name="email" placeholder="Email" required>
                    <input type="password" name="password" placeholder="Mot de passe"required>
                    <input type="submit" name="submit" value="Connexion">
                </form>
            </div>
        </div>

    </body>
</html>
