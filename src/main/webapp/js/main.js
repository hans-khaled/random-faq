const showMenu = (toggleId, navId) => {
    const toggle = document.getElementById(toggleId)
    nav = document.getElementById(navId)

    if (toggle && nav) {
        toggle.addEventListener('click', () => {
            nav.classList.toggle('show-menu');
        })
    }
}
showMenu('toggler', 'dropdown');

function removeNotif(event) {
    event.preventDefault();
    console.table(event)
    document.querySelector('#notif').remove();
}